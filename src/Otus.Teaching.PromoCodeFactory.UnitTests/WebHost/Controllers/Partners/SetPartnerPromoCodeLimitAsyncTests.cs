﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
  public class SetPartnerPromoCodeLimitAsyncTests
  {
    private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
    private readonly PartnersController _partnersController;
    private readonly SetPartnerPromoCodeLimitRequest _request;

    public SetPartnerPromoCodeLimitAsyncTests()
    {
      //TODO: Add Unit Tests
      var fixture = new Fixture().Customize(new AutoMoqCustomization());
      _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
      _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

      var autoFixture = new Fixture();
      _request = autoFixture.Create<SetPartnerPromoCodeLimitRequest>();
    }

    public Partner CreateBasePartner()
    {
      var partner = new Partner()
      {
        Id = Guid.NewGuid(),
        Name = "Test",
        IsActive = true,
        PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = DateTime.Today,
                        EndDate = DateTime.Today.AddDays(10),
                        Limit = 100
                    }
                }
      };

      return partner;
    }

    /// <summary>
    /// партнер не найден
    /// </summary>
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
    {
      // Arrange
      var partnerId = Guid.NewGuid();
      Partner partner = null;

      _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

      //Act
      var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _request);

      //Assert
      result.Should().BeAssignableTo<NotFoundResult>();
    }

    /// <summary>
    /// партнер заблокирован
    /// </summary>
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
    {
      // Arrange
      Partner partner = CreateBasePartner();
      partner.IsActive = false;

      _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

      //Act
      var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

      //Assert
      result.Should().BeAssignableTo<BadRequestObjectResult>();
    }

    /// <summary>
    /// 
    /// </summary>
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_ActiveLimitIsNotNull_ActiveLimitIsZero()
    {
      // Arrange
      Partner partner = CreateBasePartner();

      _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

      var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);

      //Act
      var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

      //Assert
      result.Should().BeAssignableTo<CreatedAtActionResult>();
      partner.NumberIssuedPromoCodes.Should().Be(0);

    }

    /// <summary>
    /// усстановка нового лимита
    /// </summary>
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_CancelLimitIsNow()
    {
      // Arrange
      Partner partner = CreateBasePartner();

      _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
          .ReturnsAsync(partner);

      var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);

      //Act
      var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

      //Assert
      result.Should().BeAssignableTo<CreatedAtActionResult>();
      activeLimit.CancelDate.Should().Be(activeLimit.CreateDate);
    }

    /// <summary>
    /// установка нового лимита больше нуля
    /// </summary>
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_SetNewLimit_LimitOverZero()
    {
      // Arrange
      Partner partner = CreateBasePartner();
      partner.PartnerLimits.FirstOrDefault().CancelDate = DateTime.Now;
      partner.NumberIssuedPromoCodes = 50;
      _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

      var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);

      //Act
      var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);

      //Assert
      result.Should().BeAssignableTo<CreatedAtActionResult>();
      partner.NumberIssuedPromoCodes.Should().BeGreaterThan(0);
    }

    /// <summary>
    /// сохранение лимита в базу и проверка
    /// </summary>
    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_SaveNewLimit_DbHasNewLimit()
    {
      // Arrange
      Partner partner = CreateBasePartner();

      _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
      _partnersRepositoryMock.Setup(repo => repo.AddAsync(partner));
      _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

      //Act
      var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, _request);
      var partnerFromDb = await _partnersRepositoryMock.Object.GetByIdAsync(partner.Id);
      var newPartnerLimit = partnerFromDb.PartnerLimits.Where(l => l.Limit == partner.PartnerLimits.FirstOrDefault().Limit).FirstOrDefault();

      //Assert
      newPartnerLimit.Should().NotBeNull();
    }

  }
}
